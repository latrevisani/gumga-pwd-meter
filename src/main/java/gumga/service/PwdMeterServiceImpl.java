package gumga.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gumga.vo.MeterResult;
import static gumga.service.Constants.*;

public class PwdMeterServiceImpl implements PwdMeterService {

	@Override
	public MeterResult checkPawword(String password) {
		MeterResult meterResult = new MeterResult();
		
		reset();

		if (password != null && password.length() > 0) {
			int score = score(password);
			String description = calculateDescription(score);

			meterResult.setPercentage(score);
			meterResult.setDescription(description);
		}

		return meterResult;
	}

	private int charUpperBonus = 0;
	private int charLowerBonus = 0;
	private int numberBonus = 0;
	private int symbolBonus = 0;
	private int midNumberOrSymbol = 0;
	private int requestedChars = 0;

	private String calculateDescription(int score) {
		if (score == 0) {
			return "Muito Curta";
		} else if (score < 20) {
			return "Muito Fraca";
		} else if (score < 40) {
			return "Fraca";
		} else if (score < 60) {
			return "Boa";
		} else if (score < 80) {
			return "Forte";
		} else {
			return "Muito Forte";
		}
	}
	
	private void reset() {
		charUpperBonus = 0;
		charLowerBonus = 0;
		numberBonus = 0;
		symbolBonus = 0;
		midNumberOrSymbol = 0;
		requestedChars = 0;
	}

	private int onlyLetters(String password) {
		Integer size = password.length();

		Pattern p = Pattern.compile("[a-zA-Z]{" + size + "}");
		Matcher m = p.matcher(password);

		if (m.matches()) {
			return -size;
		}

		return 0;
	}

	private int onlyNumbers(String password) {
		Integer size = password.length();

		Pattern p = Pattern.compile("[0-9]{" + size + "}");
		Matcher m = p.matcher(password);

		if (m.matches()) {
			return -size;
		}

		return 0;
	}

	private int repeatedChars(String password) {
		Integer size = password.length();
		Double reduction = 0.0;
		Integer repeatedChars = 0;
		Integer uniqueChars = 0;
		Boolean hasRepeated = false;

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (password.charAt(i) == password.charAt(j) && i != j) {
					hasRepeated = true;
					reduction += Math.abs(size.doubleValue() / (j - i));
				}
			}

			if (hasRepeated) {
				repeatedChars++;
				uniqueChars = size - repeatedChars;
				reduction = ((uniqueChars > 0) ? Math.ceil(reduction / uniqueChars) : Math.ceil(reduction));
			}
		}

		return -reduction.intValue();
	}

	private int sequenceChars(String password) {
		Integer inARow = 0;

		for (int i = 0; i < (ALFA.length() - 3); i++) {
			StringBuffer sFwd = new StringBuffer(ALFA.substring(i, i + 3));
			String triple = sFwd.toString();
			String tripleReverse = sFwd.reverse().toString();
			if (password.toLowerCase().indexOf(triple) != -1 || password.toLowerCase().indexOf(tripleReverse) != -1) {
				inARow++;
			}
		}

		return -(inARow * MULT_SEQ_CHAR);
	}

	private int sequenceNumbers(String password) {
		Integer consecutives = 0;

		for (int i = 0; i < (NUMERIC.length() - 2); i++) {
			StringBuffer sFwd = new StringBuffer(NUMERIC.substring(i, i + 3));
			String triple = sFwd.toString();
			String tripleReverse = sFwd.reverse().toString();
			if (password.toLowerCase().indexOf(triple) != -1 || password.toLowerCase().indexOf(tripleReverse) != -1) {
				consecutives++;
			}
		}

		return -(consecutives * MULT_SEQ_NUMERO);
	}

	private int sequenceSymbos(String password) {
		Integer consecutives = 0;

		for (int i = 0; i < (SIMBOLOS.length() - 2); i++) {
			StringBuffer sFwd = new StringBuffer(SIMBOLOS.substring(i, i + 3));
			String triple = sFwd.toString();
			String tripleReverse = sFwd.reverse().toString();
			if (password.toLowerCase().indexOf(triple) != -1 || password.toLowerCase().indexOf(tripleReverse) != -1) {
				consecutives++;
			}
		}

		return -(consecutives * MULT_SEQ_SIMBOLO);
	}

	private int consecutiveUpperChars(String password) {
		Integer size = password.length();
		Integer tmp = 0;
		Integer consecutiveUppers = 0;
		
		for (int i = 0; i < size; i++) {
			String c = String.valueOf(password.charAt(i));

			if (Pattern.compile("[A-Z]", Pattern.DOTALL).matcher(c).matches()) {
				if (i != 0) {
					if ((tmp + 1) == i) {
						consecutiveUppers++;
					}
				}
				tmp = i;
				charUpperBonus++;
			}
		}

		return -(consecutiveUppers * MULT_CONSEC_MAIUSCULAS);
	}

	private int consecutiveLowerChars(String password) {
		Integer size = password.length();
		Integer tmp = 0;
		Integer consecutiveLowers = 0;

		for (int i = 0; i < size; i++) {
			String c = String.valueOf(password.charAt(i));

			if (Pattern.compile("[a-z]", Pattern.DOTALL).matcher(c).matches()) {
				if (i != 0) {
					if ((tmp + 1) == i) {
						consecutiveLowers++;
					}
				}
				tmp = i;
				charLowerBonus++;
			}
		}

		return -(consecutiveLowers * MULT_CONSEC_MINUSCULAS);
	}

	private int consecutiveNumbers(String password) {
		Integer size = password.length();
		Integer tmp = 0;
		Integer consecutiveNumbers = 0;

		for (int i = 0; i < size; i++) {
			String c = String.valueOf(password.charAt(i));

			if (Pattern.compile("[0-9]", Pattern.DOTALL).matcher(c).matches()) {
				if (i > 0 && i < (size - 1)) {
					midNumberOrSymbol++;
				}

				if (i != 0) {
					if ((tmp + 1) == i) {
						consecutiveNumbers++;
					}
				}
				tmp = i;
				numberBonus++;
			}
		}

		return -(consecutiveNumbers * MULT_CONSEC_NUMEROS);
	}

	private int consecutiveSymbols(String password) {
		Integer size = password.length();
		Integer tmp = 0;
		Integer consecutiveSymbols = 0;

		for (int i = 0; i < size; i++) {
			String c = String.valueOf(password.charAt(i));

			if (Pattern.compile("[^a-zA-Z0-9_]", Pattern.DOTALL).matcher(c).matches()) {
				if (i > 0 && i < (size - 1)) {
					midNumberOrSymbol++;
				}

				if (i != 0) {
					if ((tmp + 1) == i) {
						consecutiveSymbols++;
					}
				}
				tmp = i;
				symbolBonus++;
			}
		}

		return -(consecutiveSymbols * MULT_CONSEC_SIMBOLOS);
	}

	private int passwordSize(String password) {
		return password.length() * 4;
	}

	private int upperChars(String password) {
		consecutiveUpperChars(password);
		return (charUpperBonus == 0) ? charUpperBonus : ((password.length() - charUpperBonus) * 2);
	}

	private int lowerChars(String password) {
		consecutiveLowerChars(password);
		return (charLowerBonus == 0) ? charLowerBonus : ((password.length() - charLowerBonus) * 2);
	}

	private int numbers(String password) {
		int bonus = 0;
		consecutiveNumbers(password);

		if (numberBonus > 0 && numberBonus < password.length()) {
			bonus = numberBonus * MULT_NUMEROS;
		}
		return bonus;
	}

	private int symbols(String password) {
		int bonus = 0;
		consecutiveSymbols(password);

		if (symbolBonus > 0) {
			bonus = symbolBonus * MULT_SIMBOLOS;
		}
		return bonus;
	}

	private int midNumbersOrSymbols(String password) {
		int bonus = 0;

		consecutiveNumbers(password);
		consecutiveSymbols(password);

		if (midNumberOrSymbol > 0) {
			bonus = midNumberOrSymbol * MULT_NUMEROS_OR_SIMBOLO;
		}
		return bonus;
	}

	private int requirement(String password) {
		int result = 0;

		consecutiveNumbers(password);
		consecutiveUpperChars(password);
		consecutiveLowerChars(password);
		consecutiveSymbols(password);

		int[] arrChars = { password.length(), charUpperBonus, charLowerBonus, numberBonus, symbolBonus };

		String[] arrCharsIds = { "nLength", "nAlphaUC", "nAlphaLC", "nNumber", "nSymbol" };
		int arrCharsLen = arrChars.length;

		for (int c = 0; c < (arrCharsLen - 1); c++) {
			int minVal = 0;

			if (arrCharsIds[c] == "nLength") {
				minVal = PASSWORD_LENGTH - 1;
			}

			if (arrChars[c] == minVal + 1) {
				requestedChars++;
			} else if (arrChars[c] > minVal + 1) {
				requestedChars++;
			}
		}

		int nMinReqChars = 0;

		if (password.length() >= PASSWORD_LENGTH) {
			nMinReqChars = 3;
		} else {
			nMinReqChars = 4;
		}

		if (requestedChars > nMinReqChars) {
			result = requestedChars * 2;
		}

		return result;
	}

	private int score(String password) {
		int score = checkBonuses(password) + checkDiscounts(password);
		
		score = (score > 100) ? 100 : (score < 0) ? 0 : score;
		
		return score;
	}

	private int checkDiscounts(String password) {
		return onlyLetters(password) + onlyNumbers(password) + repeatedChars(password) + sequenceChars(password)
				+ sequenceNumbers(password) + sequenceSymbos(password) + consecutiveUpperChars(password)
				+ consecutiveLowerChars(password) + consecutiveNumbers(password) + consecutiveSymbols(password);
	}

	private int checkBonuses(String password) {
		return passwordSize(password) + upperChars(password) + lowerChars(password) + numbers(password)
				+ symbols(password) + midNumbersOrSymbols(password) + requirement(password);
	}
}
