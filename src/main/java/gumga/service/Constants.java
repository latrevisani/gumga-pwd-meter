package gumga.service;

public class Constants {

	public static final String ALFA = "abcdefghijklmnopqrstuvwxyz";
	public static final String NUMERIC = "01234567890";
	public static final String SIMBOLOS = ")!@#$%^&*()";
	public static final Integer MULT_SEQ_CHAR = 3;
	public static final Integer MULT_SEQ_NUMERO = 3;
	public static final Integer MULT_SEQ_SIMBOLO = 3;
	public static final Integer MULT_CONSEC_MAIUSCULAS = 2;
	public static final Integer MULT_CONSEC_MINUSCULAS = 2;
	public static final Integer MULT_CONSEC_NUMEROS = 2;
	public static final Integer MULT_CONSEC_SIMBOLOS = 1;
	public static final Integer MULT_SIMBOLOS = 6;
	public static final Integer MULT_NUMEROS = 4;
	public static final Integer MULT_NUMEROS_OR_SIMBOLO = 2;
	public static final Integer PASSWORD_LENGTH = 8;

}
