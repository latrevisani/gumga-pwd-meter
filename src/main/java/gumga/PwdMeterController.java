package gumga;

import org.springframework.web.bind.annotation.RestController;

import gumga.service.PwdMeterService;
import gumga.vo.MeterResult;
import gumga.vo.Password;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/pwdmeter")
public class PwdMeterController {
    
	@Autowired
	public PwdMeterService pwdMeterService;
	
	@PostMapping
    public ResponseEntity<MeterResult> indexPost(@RequestBody(required = false) final Password pwd) {
        return getIndex(pwd.getPassword());
    }

	@GetMapping
    public ResponseEntity<MeterResult> indexGet(@RequestParam(required = false) final String  pwd) {
        return getIndex(pwd);
    }
	
	private ResponseEntity<MeterResult> getIndex(String pwd) {

		MeterResult meterResult = pwdMeterService.checkPawword(pwd);
    	
        return new ResponseEntity<>(meterResult, HttpStatus.OK);
	}
}
