package gumga.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import gumga.service.PwdMeterService;
import gumga.service.PwdMeterServiceImpl;

@Configuration
public class FactoryConfiguration {

	@Bean(name = "pwdMeterService")
	public PwdMeterService getPwdMeterService() {
		return new PwdMeterServiceImpl();
	}
}
