package gumga.vo;

import java.io.Serializable;

public class MeterResult implements Serializable {

	private static final long serialVersionUID = 2253141879008210901L;
	
	private int percentage;
	private String description;
	
	public MeterResult() {
		this.percentage = 0;
		this.description = "Muito Curta";
	}
	
	public int getPercentage() {
		return percentage;
	}
	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
