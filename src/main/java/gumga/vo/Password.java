package gumga.vo;

import java.io.Serializable;

public class Password implements Serializable {

	private static final long serialVersionUID = -3012555913556009453L;

	private String password;
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
