function checkPassword() {
	var xmlhttp = new XMLHttpRequest();
    var password = document.getElementById("password");
    var url = window.location.href + "pwdmeter";
    xmlhttp.open('POST', url, true);
    xmlhttp.setRequestHeader('Content-Type', 'application/json');
    xmlhttp.send(JSON.stringify({"password": password.value }));
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4) {
        	if ( xmlhttp.status == 200) {
        		var pwdMeter = JSON.parse(xmlhttp.responseText);
        		setResults(pwdMeter);
            } else {
            	alert("Error ->" + xmlhttp.responseText);
			}
        }
    };
}

function setResults(pwdMeter) {
	var percentage =  document.getElementById("percentage");
    var description =  document.getElementById("description");
    percentage.textContent = pwdMeter.percentage + "%";
    description.textContent = pwdMeter.description;
    
	if (pwdMeter.percentage < 1) {
		description.style.backgroundColor = 'red';
		description.style.color = 'white';
		percentage.style.color = "white";
	} else if (pwdMeter.percentage < 20) {
		description.style.backgroundColor = 'red';
		description.style.color = 'white';
		percentage.style.color = "red";
	} else if (pwdMeter.percentage < 40) {
		description.style.backgroundColor = '#E00000';
		description.style.color = 'white';
		percentage.style.color = "#E00000";
	} else if (pwdMeter.percentage < 60) {
		description.style.backgroundColor = 'yellow';
		description.style.color = 'black';
		percentage.style.color = "yellow";
	} else if (pwdMeter.percentage < 80) {
		description.style.backgroundColor = '#00FF99';
		description.style.color = 'white';
		percentage.style.color = "#00FF99";
	} else {
		description.style.backgroundColor = 'green';
		description.style.color = 'white';
		percentage.style.color = "green";
	}
}
